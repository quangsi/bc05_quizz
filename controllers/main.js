import { dataQuestion } from "../data/questions.js";
import { renderQuestion } from "./controller.js";

let currentQuestionIndex = 0;

// render lần đầu

document.getElementById("currentStep").innerHTML = `${
  currentQuestionIndex + 1
}/${dataQuestion.length}`;
renderQuestion(dataQuestion[currentQuestionIndex]);
// khi user nhấn nextq
function nextQuestion() {
  currentQuestionIndex++;
  document.getElementById("currentStep").innerHTML = `${
    currentQuestionIndex + 1
  }/${dataQuestion.length}`;

  // render nội dung câu hỏi

  renderQuestion(dataQuestion[currentQuestionIndex]);
}

window.nextQuestion = nextQuestion;
