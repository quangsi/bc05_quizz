let renderRadionButton = (data) => {
  return `<div class="form-check">
 <label class="form-check-label">
    <input type="radio" class="form-check-input" name="singleChoice" value=${data.exact} value="checkedValue" >
   ${data.content}
  </label>
</div>`;
};
let renderSingleChoice = (question) => {
  let contentAnswer = "";
  question.answers.forEach((item) => {
    let radioHTML = renderRadionButton(item);
    contentAnswer += radioHTML;
  });
  return ` <p>${question.content}</p>
  <div>
  ${contentAnswer}
  </div>
  `;
};

export const renderFillToInput = () => {
  return `<div class="form-group">
    <label for=""></label>
    <input type="text"
      class="form-control" id="" placeholder="">
  </div>`;
};
export const renderQuestion = (question) => {
  if (question.questionType == 1) {
    // render singleChoice

    document.getElementById("contentQuiz").innerHTML =
      renderSingleChoice(question);
  } else {
    // render fillInput
  }
};
